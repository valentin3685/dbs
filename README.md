# Linear Predictions on the Future

The project by Valentin, Anton and Lena at the Freie Universität Berlin for the Data Base Systems 2021 course.

## Directory overview

- `database`: The database dump and SQL scripts
- `dataset`: The dataset used in our database
- `downloads`: Files, e.g. datasets, we downloaded from external websites
- `misc`: Miscellaneous files which resulted from our project work, you may ignore this folder
- `presentation`: The presentation we gave on our project
- `webapp`: The webapp combining a frontend webpage and a backend webserver

## External Assets inside of this Repository

- Dataset, `fossil_fuel_consumption.csv`, `API_SP.URB.TOTL_DS2_en_csv_v2_2450349.zip` - World Bank, https://data.worldbank.org/indicator/SP.URB.TOTL
    - Last accessed 01.07.2021, 17:25.
- Dataset, `API_EG.USE.COMM.FO.ZS_DS2_en_csv_v2_2445714.zip` - World Bank,  https://data.worldbank.org/indicator/EG.USE.COMM.FO.ZS?view=map&year=2011
    - Last accessed 01.07.2021, 17:25.
- Favicon used, `index.png`: https://www.pinclipart.com/pindetail/iJxxJb_favicon-globe-icon-no-background-clipart/
    - Last accessed 01.07.2021, 17:25.
    - Free for personal use

## Screenshot of the Webapp

![](screenshot.png)
