\documentclass[notheorems]{beamer}

\usepackage{bookmark}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{tabularx}

\usetheme{metropolis}

\titlegraphic{\includegraphics[width=0.125\linewidth]{img/index.png}}
\title{
    Linear Predictions on the Future
}
\author{
    \normalsize Valentin Pickel, Anton Wille, Lena Nguyen\texorpdfstring{\\} {}
    \normalsize 5407070, 5093261, 5314025\texorpdfstring{\\} {}
    Department of Computer Science, Freie Universität Berlin
}
\date{5th Juli 2021{ }(newest version)}

\setlength{\parindent}{0pt}

\lstset{%
    basicstyle=\ttfamily,%
    language=SQL%
}

\begin{document}

    \begin{frame}
        \maketitle
    \end{frame}
    
    \begin{frame}{Research Question}
        \begin{itemize}
            \item Our starting point:
            
            What will the world look like in 2030?
            \item In application to this project:
            
            \textbf{How accurately can we predict future demographic and economic circumstances using linear regression?}
        \end{itemize}
    \end{frame}
    
    \begin{frame}{Additional Data Set}
        \begin{itemize}
            \item Indicator: Fossil Fuel Energy Consumption
            \item Source: World Development Indicators by the World Bank
            \item Current topic; Goes well with the \texttt{co2\_emission.csv}-file
        \end{itemize}
    \end{frame}

    \begin{frame}{UN World Predictions}
        Two metrics compared here:
        \begin{itemize}
            \item World population: 8.5 billion \footnote{\scriptsize \url{https://www.un.org/en/development/desa/population/publications/pdf/trends/Population2030.pdf}, p. 5; last access: 04.07.2021 14:06}
            \item CO2: 54-56 gigatonnes \footnote{\scriptsize \url{https://www.un.org/sustainabledevelopment/blog/2016/11/report-world-must-cut-further-25-from-predicted-2030-emissions/}, beginning; last access: 04.07.2021 14:02}
        \end{itemize}
        GDP can be compared too, other metrics a bit hard since e.g. procentual data.
    \end{frame}
    
    
    \begin{frame}{ER-Model}
        \includegraphics[width=1\linewidth]{img/ERModel.png}
    \end{frame}
    
    
    \begin{frame}{Relational Model}
        \scriptsize
        COUNTRYNAME \\
        \vspace*{0.25cm}
        \begin{tabular}{|>{\centering\arraybackslash}p{2cm}%
                        |>{\centering\arraybackslash}p{2cm}|}
            \hline
            \underline{CountryCode} & Name\\
            \hline
            \(\vdots\) & \(\vdots\)\\
            \hline
        \end{tabular}

        COUNTRY \\
        \vspace*{0.25cm}
        \begin{tabular}{|>{\centering\arraybackslash}p{2cm}%
                        |>{\centering\arraybackslash}p{2cm}%
                        |>{\centering\arraybackslash}p{2cm}%
                        |>{\centering\arraybackslash}p{3cm}|}
            \hline
            \underline{CountryCode} & \underline{Year} & CO2Emissions & FossilFuelConsumption \\
            \hline
            \(\vdots\) & \(\vdots\) & \(\vdots\) & \(\vdots\)\\
            \hline
        \end{tabular}
        \vspace*{0.25cm}
        \begin{tabular}{p{2cm}%
                        |>{\centering\arraybackslash}p{2cm}%
                        |>{\centering\arraybackslash}p{2cm}%
                        |>{\centering\arraybackslash}p{3cm}|}
                        \cline{2-4}
            & GDP & Population & PopulationGrowth\\
            \cline{2-4}
            & \(\vdots\) & \(\vdots\) & \(\vdots\)\\
            \cline{2-4}
        \end{tabular}
    \end{frame}
    
    \begin{frame}{Data Cleansing}
        \begin{itemize}
            \item Filtered out all entries < 1960
            \item Funny encoding issues like:
            \begin{itemize}
                \item "Korea, Dem. People’s Rep." being read as "Korea, Dem. Peopleâ€™s Rep.". 
                \item Cote d"Ivoire" was changed to "Cote d'Ivoire".
            \end{itemize}
            \item CountryCodes had to be normalized across files
            \item Removed Regions with missing CCs, eg. Channel Islands
            \item Created CSV files corresponding to our SQL Schema for easy import
        \end{itemize}
    \end{frame}
    
    \begin{frame}[fragile]{The Schema}
        \footnotesize
        \begin{lstlisting}
create table COUNTRY (
    CountryCode             character(3)    NOT NULL,
    Year                    int             NOT NULL,
    CO2Emissions            decimal(32, 8),
    FossilFuelConsumption   decimal(32, 8),
    GDP                     decimal(32, 8),
    Population              bigint,
    PopulationGrowth        decimal,
    primary key (CountryCode, Year));
    
create table COUNTRYNAME (
    CountryCode character(3)            UNIQUE NOT NULL,
    Name character varying(48),
    primary key (CountryCode)
);
        \end{lstlisting}
    \end{frame}

    \begin{frame}{Some basic Facts about our Implementation}

        Frameworks used:
        \begin{itemize}
            \item DB: Postgres
            \item Backend: Flask (Python) and other Pyhon modules
            \item Frontend: VueJS + HTML + CSS 
            \item Visualization: Chart.js + Datamaps
        \end{itemize}

    \end{frame}


    \begin{frame}{Some basic Facts about our Implementation}
        Performance and Miscellaneous
        \begin{itemize}
            \item HTTPS with local certificates with \texttt{mkcert}, encrypting is good practice in our opinion
            \item \texttt{HTTP/2.0} for persistent connections \(\Rightarrow\) Massive speed improvements due to our requirements, see our animation feature in the live-demo
            \item Large amounts of data due to visualization (up to 4kB) \(\Rightarrow\) \texttt{Gzip}-compressed responses, can be handled with HTTP proxy too, e.g. \texttt{nginx}
        \end{itemize}
    \end{frame}
    
    \begin{frame}{Effect of the Optimizations}
        \begin{center}
            \includegraphics[width=0.49\linewidth]{img/without_optimization.png}
            \includegraphics[width=0.49\linewidth]{img/with_optimization.png}
        \end{center}
    \end{frame}

    \begin{frame}
        \begin{center}
            \Huge
            Live Demonstration
            \normalsize

            Our code repository: \url{https://git.imp.fu-berlin.de/valentin3685/dbs}
        \end{center}
    \end{frame}


    \begin{frame}{Our Backend: Routes (Extra)}
        Important Flask API Routes (besides static page serving)

        \begin{itemize}
            \item \texttt{/CountryCodes}
            \item \texttt{/<int:year>/<string:dim>}
            \item \texttt{/<string:code>/<string:dim>/<int:beg>}
            \item \texttt{/<string:code>/<string:dim>/<int:beg>-<int:end>}
            \item \texttt{/<string:code>/<string:dim>/<int:beg>-<int:end>/<string:op>}
        \end{itemize}
    \end{frame}

\end{document}
