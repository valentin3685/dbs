-----------------------------------------------
-- Database Systems SoSe 2021                --
-- Valentin Pickel, Anton Wille, Lena Nguyen --
-- Institute for Compiter Science            --
-- Freie Universität Berlin                  --
-----------------------------------------------

-- Creates the tables for our database.
-- Create a database, preferable called "dbs" and execute these commands.

create table COUNTRY (
    -- Country codes always made up of 3 characters, e.g. DEU
    CountryCode             character(3)    NOT NULL,
    -- Not bigint, but no problem in this case; Might also be negative for values BC
    Year                    int             NOT NULL,
    -- Variable precision, 32 should be more than enough
    CO2Emissions            decimal(32, 8),
    FossilFuelConsumption   decimal(32, 8),
    GDP                     decimal(32, 8),
    -- Big int for "World" meta entry (> 8 billion, thus need 64 bits here)
    Population              bigint,
    PopulationGrowth        decimal,
    primary key (CountryCode, Year)
);

create table COUNTRYNAME (
    CountryCode character(3)               UNIQUE NOT NULL,
    -- 64 arbitrarily chosen
    Name character varying(48),
    primary key (CountryCode)
);
