-----------------------------------------------
-- Database Systems SoSe 2021                --
-- Valentin Pickel, Anton Wille, Lena Nguyen --
-- Institute for Compiter Science            --
-- Freie Universität Berlin                  --
-----------------------------------------------

-- Imports the data from our combined normalized datatable "country_codes.csv".
-- Execute these commands while being in the "dataset/normalized" folder and after having executed all instructions in `create.sql` on your database cluster.

COPY COUNTRYNAME(CountryCode, Name)
FROM country_codes.csv 
DELIMITER ',' 
CSV HEADER;

COPY COUNTRY
FROM combined_table.csv
DELIMITER ','
CSV HEADER;

-- Alternatively, if the above commands do not work:
\copy CountryName from country_codes.csv delimiter ',' csv header;
\copy Country from combined_table.csv delimiter ',' csv header;
