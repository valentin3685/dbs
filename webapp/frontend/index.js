"use strict";

/*********************************************/
/* Database Systems SoSe 2021                */
/* Valentin Pickel, Anton Wille, Lena Nguyen */
/* Institute for Compiter Science            */
/* Freie Universität Berlin                  */
/*********************************************/

const Dimension = {
    CO2Emissions: "CO2Emissions",
    FossilFuelConsumption: "FossilFuelConsumption",
    GDP: "GDP",
    Population: "Population",
    PopulationGrowth: "PopulationGrowth"
};

var countrySelected = "DEU";
var dimensionSelected = Dimension.CO2Emissions;
var startYear = 2000;
var endYear = 2014;

var countryCodes = {};
var app = null;
var animateButton = null;
var map = null;
var scatterChart = null;

// As the Datamaps library is based on TopoJSON, we may directly edit properties
var geo = null;

const countryHoverColor = "#000000";

// Async function for retrieving first data
(async function() {
    // Obtain the list of country codes and the names
    countryCodes = await fetch("/CountryCodes").then(response => response.json());

    const datamapsConfig = {
        element: document.getElementById('worldMap'),
        fills: {
            // Only display countries in country code list fetched afterwards
            defaultFill: "#FFFFFF"
        },
        geographyConfig: {
            highlightFillColor: 'rgba(0, 0, 0, 1)',
            highlightBorderColor: '#FFFFFF'
        },
        // For a prettier projection
        projection: 'mercator',
        responsive: true,
        // Attach event handlers to the countries
        done: function (datamap) {
            datamap.svg.selectAll(".datamaps-subunit").on("click", countryClicked);
        }
    }
    map = new Datamap(datamapsConfig);

    window.addEventListener("resize", function() {
        map.resize();
    });

    geo = map.worldTopo.objects.world.geometries;
    geo.forEach(g => {
        g.properties.name = countryCodes[g.id];
    });

    app = new Vue({
        el: "#app",
        data: {
            country: countryCodes[countrySelected] + " (" + endYear + ")",
            correlationCoefficient: "0",
            CO2Emissions: "0",
            FossilFuelConsumption: "0",
            GDP: "0",
            Population: "0",
            PopulationGrowth: "0"
        }
    });
    animateButton = new Vue({
        el: "#animateButtonWrapper"
    });
    
    const scatterChartConfig = {
        type: "line",
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        // Display all x labels
                        autoSkip: false
                    }
                }]
            }
        }
    };
    scatterChart = new Chart(document.getElementById("scatterChart"), scatterChartConfig);
    //scatterChart.options.animation["duration"] = 50;
    scatterChart.options.animation = false;
    updateVisualization();
})();

/********************* EVENT HANDLERS *********************/
function worldButtonClicked() {
    countryClicked(null);
}

function countryFormButtonClicked() {
    // Find the country on the map and pass it to the update functions
    var f = geo.filter(g => g.id == countrySelected);
    countryClicked(f[0]);
}

function countryClicked(geography) {
    if (!areYearsValid()) {
        alert("Enter a valid date");
        return;
    }

    //let id = geography.id;
    let name = "";

    if (geography == null) {
        name = "World";
        countrySelected = "WLD";
    }
    else {
        name = geography.properties.name;
        countrySelected = geography.id;
    }
    app.country = name + " (" + endYear + ")";
    updateVisualization();
}
/**********************************************************/

function areYearsValid() {
    if (startYear < 0 || endYear < 0 || (endYear >= 0 && startYear > endYear)) {
        return false;
    }
    return true;
}

async function updateVisualization() {
    // Set chloropleth
    startYear = parseInt(startYear)
    endYear = parseInt(endYear)

    let url = `/${endYear}/${dimensionSelected}`;
    fetch(url).then(response => response.json()).then(dimensionValues => {
        let updateValues = {};
    
        let maxValue = 0.0;
        // Percentage data
        if (dimensionSelected == Dimension.FossilFuelConsumption || dimensionSelected == Dimension.PopulationGrowth) {
            maxValue = 100.0;
        }
        else {
            for (const code in countryCodes) {
                if (code in dimensionValues) {
                    let value = parseFloat(dimensionValues[code]["data"]);
                    // Ignore world maximum for this visualization
                    if (maxValue < value & code != "WLD") {
                        maxValue = value;
                    }
                }
            }
        }
        for (const code in countryCodes) {
            if (code in dimensionValues) {
                let value = dimensionValues[code]["data"];
                if (value == null) {
                    updateValues[code] = "rgba(65, 65, 65, 0.5)";
                }
                else {
                    updateValues[code] = "rgba(2, 127, 255, " + parseFloat(value) / maxValue + ")";
                }
            }
        }
        map.updateChoropleth(updateValues);
    });

    // Set chart
    url = `/${countrySelected}/${dimensionSelected}/${startYear}-${endYear}`;
    fetch(url).then(response => response.json()).then(data => {
        scatterChart.data.labels = [];
        scatterChart.data.datasets = [];;
        let m = parseFloat(data["regM"]);
        let n = parseFloat(data["regN"]);
        let r = parseFloat(data["regR"]);

        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from
        let labels = Array.from({length: endYear + 1 - startYear}, (_, year) => startYear + year);
        let dataset = [];
        let predicted = [];
        for (let i = startYear; i <= endYear; i++) {
            let {"data": d, "predicted": p} = data.data[i - startYear];
            if (!p) {
                dataset.push({x: i, y: d});
            }
            else {
                predicted.push({x: i, y: d});
            }
        }
        var data = {
            labels: labels,
            datasets: [
                {
                    label: "Data",
                    data: dataset,
                    backgroundColor: "#0000FF",
                    showLine: false
                },
                {
                    label: "Predicted",
                    data: predicted,
                    backgroundColor: "#FFFF00",
                    showLine: false
                },
                {
                    label: "Linear Regression",
                    data: [
                        {x: startYear, y: m * startYear + n},
                        {x: endYear, y: m * endYear + n}
                    ],
                    backgroundColor: "#FF0000",
                    borderColor: "#FF0000",
                    showLine: true
                }
            ]
        };
        scatterChart.data = data;
        app.correlationCoefficient = r;
        scatterChart.update();
    });

    // Set average table

    // Backticks for formatting
    const avg_url = [
        `/${countrySelected}/${Dimension.CO2Emissions}/${startYear}-${endYear}/avg`,
        `/${countrySelected}/${Dimension.FossilFuelConsumption}/${startYear}-${endYear}/avg`,
        `/${countrySelected}/${Dimension.GDP}/${startYear}-${endYear}/avg`,
        `/${countrySelected}/${Dimension.Population}/${startYear}-${endYear}/avg`,
        `/${countrySelected}/${Dimension.PopulationGrowth}/${startYear}-${endYear}/avg`
    ];

    fetch(avg_url[0]).then(response => response.json()).then(CO2 => {
    fetch(avg_url[1]).then(response => response.json()).then(FFC => {
    fetch(avg_url[2]).then(response => response.json()).then(GDP => {
    fetch(avg_url[3]).then(response => response.json()).then(Pop => {
    fetch(avg_url[4]).then(response => response.json()).then(PopGro => {
        app.CO2Emissions            = "error" in CO2    ? 0.0 : CO2   ["data"][0]["data"].toFixed(2);
        app.FossilFuelConsumption   = "error" in FFC    ? 0.0 : FFC   ["data"][0]["data"].toFixed(2);
        app.GDP                     = "error" in GDP    ? 0.0 : GDP   ["data"][0]["data"].toFixed(2);
        app.Population              = "error" in Pop    ? 0.0 : Pop   ["data"][0]["data"].toFixed(2);
        app.PopulationGrowth        = "error" in PopGro ? 0.0 : PopGro["data"][0]["data"].toFixed(2);
    })})})})});
}

function animateButtonClicked() {
    if (!areYearsValid()) {
        alert("Enter a valid date");
        return;
    }

    const animateCountrySelected = countrySelected;
    const animateDimensionSelected = dimensionSelected;
    const animateStartYear = startYear;
    const animateEndYear = endYear;
    let animateYear = startYear;

    let intervalHandle = window.setInterval(async function() {
        countrySelected = animateCountrySelected;
        dimensionSelected = animateDimensionSelected;
        startYear = animateStartYear;
        endYear = animateYear;
        await countryFormButtonClicked();

        animateYear++;
        if (endYear == animateEndYear) {
            window.clearInterval(intervalHandle);
        }
    }, 100);
}
