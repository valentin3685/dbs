# Script for setting up the development environment

# For debugging support, such as reloading the webserver as soon as we change something in the backend
$env:FLASK_APP="backend/app.py"
$env:FLASK_DEBUG=1
$env:FLASK_ENV="development"
mkcert -install
mkdir cert
cd cert
mkcert localhost
cd ..
