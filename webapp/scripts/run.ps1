# For starting the database server and the backend server

pg_ctl -l "backend/log" start
flask run -h localhost -p 5000 --cert="cert/localhost.pem" --key="cert/localhost-key.pem"
