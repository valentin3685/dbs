#!/usr/bin/python3

#############################################
# Database Systems SoSe 2021                #
# Valentin Pickel, Anton Wille, Lena Nguyen #
# Institute for Compiter Science            #
# Freie Universität Berlin                  #
#############################################

import atexit
import flask
import psycopg2

from flask_compress import Compress
from math import sqrt
from werkzeug.serving import WSGIRequestHandler

# ===== CONSTANTS START =====

# Port of the PostgreSQL database (5432 is standard)
DB_PORT = 5432

# First prediction date - Marks the first year at which we have no data anywhere
FIRST_PRED_DATE = 2019

# ===== CONSTANTS END =====

# ===== HELPER FUNCTIONS START =====
def linear_regression(X, Y):
    """ Executes the simple linear regression formula from the lecture.
        Returns for the slope mx+n the coordinates (m, n) and the correlation coefficient r.
    """

    assert(len(X) == len(Y))

    if len(X) == 0:
        return (0, 0, 0)
    elif len(X) == 1:
        return (0, X[0], 0)

    x_mean = sum(X) / len(X)
    y_mean = sum(Y) / len(Y)

    a = sum([(x_i - x_mean) * (y_i - y_mean) for (x_i, y_i) in zip(X, Y)])
    b = sum([(x_i - x_mean)**2 for x_i in X])
    c = sum([(y_i - y_mean)**2 for y_i in Y])
    m = a / b
    n = y_mean - m * x_mean
    r = 0.0
    if c != 0.0:
        r = a / (sqrt(b) * sqrt(c))

    return (m, n, r)


def compute_regression(cursor, code, dim):
    """
        To obtain a linear regression for one country and one dimension.
    """

    if code == "WLD":
        # NOTE: Ignore other world entries for simplicity
        # NOTE: Does not consider countries who have no entries in this timespan for simplicity
        #  -> Meaning that if e.g. Aruba has no entry in 2020, its population prediction will be ignored here.
        # Gives an approximation.
        cursor.execute("select year, sum(" + dim.lower() +
                       ") from country group by year;")
    else:
        # Precompute linear regression for non-existing values - If no regression can be obtained, then the code has to be WLD with dimension Population
        cursor.execute("select year, " + dim.lower() +
                       " from country where countrycode=%s;", (code,))

    regression_data = cursor.fetchall()
    regression_data = { y: float(d) for (y, d) in regression_data if d != None }
    (m, n, r) = linear_regression(
        list(regression_data.keys()), list(regression_data.values()))
    return (m, n, r)

# Checking validity of country codes, dimensions and timespans
# - Negative timespans not allowed
# - Timespans with end < beg not allowed
def is_code_valid(code): return code in country_codes.keys()
def is_dim_valid(dim): return dim in DIMENSIONS
def is_timespan_valid(beg, end): return beg >= 0 and beg <= end

# ===== HELPER FUNCTIONS END =====

# ===== INITIALIZATION START =====

# https://stackoverflow.com/questions/10523879/how-to-make-flask-keep-ajax-http-connection-alive
WSGIRequestHandler.protocol_version = "HTTP/2.0"

# The app itself
app = flask.Flask(__name__)

# Compress responses with gzip, most browsers and clients support this.
# This code may get deleted if the app is deployed on some server, as HTTP proxies like nginx often do this themselves.
app.config["COMPRESS_ALGORITHM"] = "gzip"
# Reasonable minimal size for compression
app.config["COMPRESS_MIN_SIZE"] = 512
app_compress = Compress(app)
app.config["COMPRESS_MIMETYPES"].append("image/png")

# A databse connection, prefetch memory for storing all country codes + their names (for the /CountryCodes route)
# and an array for all linear regressions so that they can be prefetched too.
connection = None
country_codes = {}
linear_regressions = {}
DIMENSIONS = ["CO2Emissions", "FossilFuelConsumption",
              "GDP", "Population", "PopulationGrowth"]

print("INITIALIZE CONNECTION TO DB")
connection = psycopg2.connect(
    "dbname=dbs user=postgres port={}".format(DB_PORT))

def cleanup():
    print("CLOSING CONNECTION TO DB")
    connection.close()

atexit.register(cleanup)

# Prefetch the country code table and all linear regressions
with connection.cursor() as cursor:
    cursor.execute("select * from countryname;")
    codes = cursor.fetchall()
    for code in codes:
        country_codes[code[0]] = code[1]
        linear_regressions[code[0]] = {
            DIMENSIONS[0]: compute_regression(cursor, code[0], DIMENSIONS[0]),
            DIMENSIONS[1]: compute_regression(cursor, code[0], DIMENSIONS[1]),
            DIMENSIONS[2]: compute_regression(cursor, code[0], DIMENSIONS[2]),
            DIMENSIONS[3]: compute_regression(cursor, code[0], DIMENSIONS[3]),
            DIMENSIONS[4]: compute_regression(cursor, code[0], DIMENSIONS[4])
        }

# ===== INITIALIZATION END =====

@app.route("/")
@app.route("/index.html")
def index():
    resp = flask.send_from_directory("../frontend", "index.html")
    resp.mimetype = "text/html"
    return resp


@app.route("/index.css")
def index_css():
    resp = flask.send_from_directory("../frontend", "index.css")
    resp.mimetype = "text/css"
    return resp


@app.route("/<path:file>.js")
def serve_js(file):
    resp = flask.send_from_directory("../frontend", file + ".js")
    resp.mimetype = "application/javascript"
    return resp


@app.route("/index.png")
def index_ico():
    resp = flask.send_from_directory("../frontend", "index.png")
    resp.mimetype = "image/png"
    return resp


@app.route("/CountryCodes")
def codes():
    return flask.jsonify(country_codes)


@app.get("/<int:year>/<string:dim>")
def serve_the_world(year, dim):
    """
        Returns all countries dimensional values of a year (for visualization e.g.)
    """

    if year < 0:
        return flask.jsonify({"error": "Invalid timespan"}), 404
    if not is_dim_valid(dim):
        return flask.jsonify({"error": "Invalid dimension"}), 404

    data = []
    with connection.cursor() as cursor:
        sql = "select countrycode, " + dim + " from country where year=%s;"
        cursor.execute(sql, (year,))
        data = dict(cursor.fetchall())

    for code in country_codes:
        if code not in data.keys() or data[code] == None:
            if year >= FIRST_PRED_DATE:
                (m, n, _) = linear_regressions[code][dim]
                data[code] = { "data": m * year + n, "predicted": True }
            else:
                data[code] = { "data": None, "predicted": False }
        else:
            data[code] = { "data": float(data[code]), "predicted": False }

    return flask.jsonify(data)


@app.get("/<string:code>/<string:dim>/<int:beg>")
@app.get("/<string:code>/<string:dim>/<int:beg>-<int:end>")
@app.get("/<string:code>/<string:dim>/<int:beg>-<int:end>/<string:op>")
def serve_year(code, dim, beg, end=None, op=""):
    """
        Serves one or more dimension values for a specific countries within a timespan and may perform operation on the values.
        Supports serving year queries, year range queries or year range queries and then applying an operation.
        Possible operations are:
            "avg" - Take average of years
    """
    if end != None:
        if not is_timespan_valid(beg, end):
            return flask.jsonify({"error": "Invalid timespan"}), 404
    else:
        end = beg
    if not is_code_valid(code):
        return flask.jsonify({"error": "Invalid country code"}), 404
    if not is_dim_valid(dim):
        return flask.jsonify({"error": "Invalid dimension"}), 404

    (regM, regN, regR) = linear_regressions[code][dim]
    cursor = connection.cursor()

    # Query for the timespan and fill in nonexisting entries with predictions

    # NOTE: dim has been verified by is_dim_valid, so there is no danger here.
    # The problem is that if dim is inserted as 'dim' into the string, PostgreSQL computes no actual table,
    # but e.g. 'CO2Emissions' as result.
    result = []
    with connection.cursor() as cursor:
        sql = ""
        if code == "WLD":
            # In case of percent data, take average - FossilFuelConsumption and PopulationGrowth are percentage data
            # TODO: Fix this properly - Actually return average world population growth & world fossil fuel consumption growth
            if (dim == DIMENSIONS[1] or dim == DIMENSIONS[4]):
                sql = "select * from (select year, avg(" + dim.lower() + \
                    ") from country group by year) as world where year>=%s and year <=%s;"
            else:
                sql = "select * from (select year, sum(" + dim.lower() + \
                    ") from country group by year) as world where year>=%s and year <=%s;"
            cursor.execute(sql, (beg, end,))
        else:
            sql = "select year, " + \
                dim.lower() + " from country where countrycode=%s and year>=%s and year <=%s;"
            cursor.execute(sql, (code, beg, end,))
        result = cursor.fetchall()

    if op == "avg":
        # Take the average over the timespan, predicting values if they are >= FIRST_PRED_DATE
        result = dict(result)
        temp = []
        for year in range(beg, end+1):
            # If value exists, append;
            if year in result.keys() and result[year] != None:
                temp.append(float(result[year]))
            elif year >= FIRST_PRED_DATE:
                pred = regM * year + regN
                # If prediction gets negative, then something is wrong. Set it to zero.
                if pred < 0.0:
                    pred = 0.0
                # Percentage data will be predicted to 100.0 if it gets larger than 100%
                if dim == DIMENSIONS[1] and pred > 100.0:
                    pred = 100.0
                temp.append(pred)
        result = temp
        # For consistency
        avg = 0.0
        if len(result) > 0:
            avg = sum(result) / len(result)
        r = {"data": [{ "data": avg }]}
        return flask.jsonify(r)
    else:
        # From the result from the DB: store the data in a dictionary form (for easy object access) and mark predicted data
        result = dict(result)
        r = {}
        temp = []
        for year in range(beg, end+1):
            if year in result.keys() and result[year] != None:
                temp.append({"data": float(result[year]), "predicted": False})
            elif year >= FIRST_PRED_DATE:
                pred = regM * year + regN
                if pred < 0.0:
                    pred = 0.0
                if dim == DIMENSIONS[1] and pred > 100.0:
                    pred = 100.0
                # Also consider "WLD" values as predicted here
                temp.append({"data": pred, "predicted": True})
            else:
                temp.append({"data": None, "predicted": False})
        r["data"] = temp
        r["regM"] = regM
        r["regN"] = regN
        r["regR"] = regR
        return flask.jsonify(r)
