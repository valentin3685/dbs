# The Projects Webapp

## API Reference (in Flask notation)

The routes (URLs) the browser takes to communicate with the webpage. We can also access them with other tools, e.g. `curl`.

- `/CountryCodes`: Obtain the list of country codes and names used in the application.
- `/<int:year>/<string:dim>`: Obtain dimension values for all countries in a specific year.
- `/<string:code>/<string:dim>/<int:beg>`: Obtain the dimension value for a country in a year.
- `/<string:code>/<string:dim>/<int:beg>-<int:end>`: Obtain the dimension values for a country in a timespan.
- `/<string:code>/<string:dim>/<int:beg>-<int:end>/<string:op>`: Obtain the dimension values for a country in a timespan and apply an operation to it. Currently, only taking the average (`op == "avg"`) is supported.

`year`, `beg` and `end` are year values; `dim` a dimension of our data (e.g. Population) and `code` a country code (3 letters).

So for instance:
- `/<code>/CO2Emissions/<Either a specific year or an interval (start-end)>`
- `/<code>/FossilFuelConsumption/<Either a specific year or an interval (start-end)>`
- `/<code>/GDP/<Either a specific year or an interval (start-end)>`
- `/<code>/Population/<Either a specific year or an interval (start-end)>`
- `/<code>/PopulationGrowth/<Either a specific year or an interval (start-end)>`

For unknown years, we use our predicition model (the linear regression) to serve predictive data, which we mark with a key `predicted` in the JSON that is returned to the client. We predict as long as the date is `>= FIRST_PRED_DATE`, see `app.py` for this value (usually 2019). We also give the constraints:

- If prediction is negative, zero is returned.
- Dates must be nonnegative.
- Percentage data is bound to 0% or 100%, see `app.py` for the calculation.
- Values in the returned data may be `null`; That is the case if the date was before `FIRST_PRED_DATE`, meaning that we do not know anything about this request.

An example JSON answer. The request:
```sh
$ curl -k https://localhost:5000/WLD/Population/2017-2018
```
Returns:
```json
{
  "data": [
    {
      "data": 7483856430.0,
      "predicted": false
    },
    {
      "data": 7524947683.072601,
      "predicted": true
    }
  ],
  "regM": 80312426.74997695,
  "regN": -154545529498.3809,
  "regR": 0.999510209288371
}
```

In some cases an error-object with the `"error"`-property is returned. Possible codes:

- `"No data available for regression"`
- `"Invalid timespan"`
- `"Invalid country code"`
- `"Invalid dimension"`

Or you receive a general 404 error from the backend.

## Running the Webapp on your Windows Computer

Dependencies:
- PostgreSQL, install with e.g. [scoop](https://scoop.sh/)
- Python, install with a package manager such as `pacman` in [MSYS2](https://www.msys2.org/) or just with `pip --user`, with:
    - flask
    - flask-compress
    - psycopg2
    - pyopenssl
- npm, install dependencies automatically by running `$ npm update` in `webapp`
- mkcert, install with e.g. `scoop` or [brew](https://brew.sh/)
- Some modern web browser, e.g. `ungoogled-chromium`

For executing, run, in the Windows PS (you may need some executive policies to be set):
```
- Import the database into a postgre database "dbs".
  We have dumped it in the `database` directory.
- $ npm update
- $ ./scripts/setenv.ps1
- $ ./scripts/run.ps1
```

And for cleanup execute:
```
$ ./scripts/unsetenv.ps1`
```

We have tested analogous procedures in Linux, some tools are usually named differently there, e.g. `pg_ctl` is usually called `pg_ctlcluster` and commands usually have to be executed as `sudo -u postgres`.
