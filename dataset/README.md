## Datasets

- `co2_emission.csv`
    - CO2 Emissions in tons from several countries, continents and the entire world.
    - Timespan: 1751-2017
    - -> "statistical differences"?! Removed this data.
    - Not all countries are listed, some may have not existed yet (also holds for the other tables).

- `gdp.csv`
    - GDP of many countries from 1960 to 2020 + World + "Not classified" + etc..
    - Timespan: 1960-2020

- `population_growth.csv`
    - Population growth in annual % (per year); again, from several countries and some "meta countries".
    - Timespan: 1960-2020

- `population_total.csv`
    - Total population sizes.
    - Timespan: 1960-2017

## Possible Augmentations

- `fossil_fuel_consumption.csv`
    - Fossil Fuel Energy Consumption in % of total energy sources. (chosen augmentation)
    - Timespan: 1960-2014
    - Source: https://data.worldbank.org/indicator/EG.USE.COMM.FO.ZS

- `urbanization.csv`
    - Percentage of people living in Urban areas.
    - Timespan: 1960-2019
    - Source: https://data.worldbank.org/indicator/SP.URB.TOTL

## Possible Questíons

- Does CO2 cost lives?
    - Suggested data for this: https://www.kaggle.com/sansuthi/life-expectancy
- How will the world look like in 2030? (chosen question)
