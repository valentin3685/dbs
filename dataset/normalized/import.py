#!/usr/bin/python3

#############################################
# Database Systems SoSe 2021                #
# Valentin Pickel, Anton Wille, Lena Nguyen #
# Institute for Compiter Science            #
# Freie Universität Berlin                  #
#############################################

import csv

def makeCombinedTable():        
    combined_table = open('combined_table.csv', 'w', newline='')
    country_codes = open('country_codes.csv', 'r')

    #Init our target File
    combinedDict = csv.DictWriter(combined_table, fieldnames=('CountryCode', 'Year', 'CO2Emissions', 
    'FossilFuelConsumption', 'GDP', 'Population', 'PopulationGrowth'), delimiter=",", quotechar="\"", restval='')
    combinedDict.writeheader()

    tuples = []
    #Create row for Cross Product of CountryCode and Year in our range
    countrycodes = csv.DictReader(country_codes, fieldnames=('CountryCode', 'Name'), delimiter=",", quotechar="\"")        
    for row in countrycodes:
        for i in range(1960, 2020):
            tuples.append({'CountryCode': row['CountryCode'], 'Year': str(i)})
    country_codes.close()

    co2 = open('co2_emission.csv', 'r')
    fossilfuel = open('fossil_fuel_consumption.csv', 'r')
    gdp = open('gdp.csv', 'r')
    poptotal = open('population_total.csv')
    popgrowth =open('population_growth.csv', 'r')

    co2Dict = csv.DictReader(co2, fieldnames=('Name', 'CountryCode', 'Year', 'Value'), delimiter=",", quotechar="\"")
    ffDict = csv.DictReader(fossilfuel, fieldnames=('CountryCode', 'Year', 'Value'), delimiter=",", quotechar="\"")
    gdpDict = csv.DictReader(gdp, fieldnames=('CountryCode', 'Year', 'Value'), delimiter=",", quotechar="\"")
    popTDict = csv.DictReader(poptotal, fieldnames=('CountryCode', 'Year', 'Value'), delimiter=",", quotechar="\"")
    popGDict = csv.DictReader(popgrowth, fieldnames=('CountryCode', 'Year', 'Value'), delimiter=",", quotechar="\"")

    for row in co2Dict:
        #Add CO2
        for i in tuples:
            if i['CountryCode'] == row['CountryCode'] and str(i['Year']) == row['Year']:
                i['CO2Emissions'] = row['Value']
                break
        #Add FossilFuel-Emissions
    
    for row in ffDict:
        for i in tuples:
            if i['CountryCode'] == row['CountryCode'] and str(i['Year']) == row['Year']:
                i['FossilFuelConsumption'] = row['Value']
                break
    
    #add GDP data
    for row in gdpDict:
        for i in tuples:
            if i['CountryCode'] == row['CountryCode'] and str(i['Year']) == row['Year']:
                i['GDP'] = row['Value']
                break

    #add Pop Total data
    for row in popTDict:  
        for i in tuples:
            if i['CountryCode'] == row['CountryCode'] and str(i['Year']) == row['Year']:
                i['Population'] = row['Value']    
                break

        #add Pop Growth data
    for row in popGDict:
        for i in tuples:
            if i['CountryCode'] == row['CountryCode'] and str(i['Year']) == row['Year']:
                i['PopulationGrowth'] = row['Value']
                break

    for i in tuples:
        combinedDict.writerow(i)

    combined_table.close()
    co2.close()
    fossilfuel.close()
    gdp.close()
    poptotal.close()
    poptotal.close()
    popgrowth.close()

    return True

if __name__=='__main__':
    makeCombinedTable()
