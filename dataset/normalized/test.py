dictlist = [{'a': 'b', 'c':'d'}, {'a': 'd', 'c':'d'}, {'a': 'e', 'c':'d'}]
ex = {'a': 'b'}

for i in dictlist:
    if i['a'] == ex['a']:
        i['CO2Emissions'] = 'x'

print(dictlist)
