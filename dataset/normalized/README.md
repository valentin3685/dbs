## Information on our Normalization Procedures

This folder contains:
- All of the datasets in their normalized form
- A script `normalize.py` that contains all functionality we needed for creating these normalized datasets
- Also, we have the table `country_codes.csv`, generated from the codes from `co2_emission.csv` and from `population_total_names_to_codes.csv`. It has been manually checked up (about 200 entries) to allow easier conversion of the country names there into country codes. The other tables have country codes.
- A script `import.py` for generating the `combined_table.csv` for an easy import into our database
- The `test.py` script may be ignored

It is needed to extract the ISO-3611-alpha2 codes from files from the dataset as these files use different names for the countries themselves. This is the reason why we need a mapping for `population_total.csv` too, as it does not contain the country codes.

`country_codes.csv` also has the special value "World" with which we denote worldwide values. It has the code `WLD`. We renamed OWID_WRL into WLD for each tuple in `co2_emission.csv`. As we did not use these entries in our application later on, these entries can also be skipped.

There were two funny encoding issues: "Korea, Dem. People’s Rep." being read as "Korea, Dem. Peopleâ€™s Rep." and "Cote d"Ivoire" being read wrong due to the double quotation mark inside. These issues we fixed by replacing the ’ and " characters inside by '. We used VSCodes find-and-replace and comparison-of-files utilities.

With the normalization, importing the data into the DB worked.

We also filtered out all entries < 1960. For consistency, as only `co2_emission` had entries further behind.

There were some country names in `population_total.csv` which do not match to any code:
- Channel Islands
- Pacific island small states
- Caribbean small states
We ignored these countries in the final DB.

For e.g. Kosovo (a European Code XRX exists, but not in the standard as it seems)

In the beginning it was considered to use AWK for these text processing task and even to parallelize the task, but using Python seemed more suitable for our data size.

## Further Inconsistencies in the Data

- Meta tuples and comments => Delete them, only a few
- CRLF, UTF-8
- Countries whose names have a comma in them
- Remove empty rows
