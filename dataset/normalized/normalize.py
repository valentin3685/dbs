#!/usr/bin/python3

#############################################
# Database Systems SoSe 2021                #
# Valentin Pickel, Anton Wille, Lena Nguyen #
# Institute for Compiter Science            #
# Freie Universität Berlin                  #
#############################################

# Normalization process by doing the following:
# - Manually deleted title rows
# - Saved CVS files as UTF-8 + CRLF for conformity, also newlines on end of files -> VSCode or dos2unix e.g.
# - This script takes fossil_fuel_consumption.csv, gdp.csv, population_growth.csv and splits up the tuples into multiple ones
# - Script generates a list of country names from co2_emissions.csv
#   -> Delete all tuples with country codes not in the table + Delete historic countries, e.g. Czechoslovakia
#   + Delete OWID-Countries, except for the World -> WLD
#   -> Generate country_codes.csv from this, codes conform to ISO-3166 https://www.iban.com/country-codes
# - As population_total.csv does not have country codes and uses slightly names for some of the countries, we mapped
#   these names to codes whenever possible and replaced them programmatically

import csv


def split_up_long_tuples(filepath):
    """ NOTE: ONLY DEVELOPED FOR THIS USE CASE
    """

    lines = []
    with open(filepath, "r") as file:
        reader = csv.reader(file, delimiter=",", quotechar="\"")
        for row in reader:
            code = row[1]
            for year in range(1960, 2020+1):
                value = row[4 + (year - 1960)]
                if value != "":
                    lines.append("\"{}\",\"{}\",\"{}\"\n".format(
                        code, str(year), value))

    with open(filepath, "w") as file:
        for line in lines:
            file.write(line)


def get_one_country_table(filepath):
    """ NOTE: ONLY DEVELOPED FOR THIS USE CASE
    """

    lines = set()
    with open(filepath, "r") as file:
        reader = csv.reader(file, delimiter=",", quotechar="\"")
        for row in reader:
            if row[1] != "":
                lines.add((row[0], row[1]))

    for line in lines:
        print(line)


def print_population_total_countries():
    """ NOTE: ONLY DEVELOPED FOR THIS USE CASE
    """

    lines = set()
    with open("population_total.csv", "r") as file:
        reader = csv.reader(file, delimiter=",", quotechar="\"")
        for row in reader:
            lines.add(row[0])

    for line in lines:
        print(line)


def population_total_replace_country_names():
    """ NOTE: ONLY DEVELOPED FOR THIS USE CASE
    """

    population_total_names_to_codes = {
        "Aruba": "ABW",
        "Afghanistan": "AFG",
        "Angola": "AGO",
        "Albania": "ALB",
        "Andorra": "AND",
        "United Arab Emirates": "ARE",
        "Argentina": "ARG",
        "Armenia": "ARM",
        "Samoa": "ASM",
        "Antigua and Barbuda": "ATG",
        "Australia": "AUS",
        "Austria": "AUT",
        "Azerbaijan": "AZE",
        "Burundi": "BDI",
        "Belgium": "BEL",
        "Benin": "BEN",
        "Burkina Faso": "BFA",
        "Bangladesh": "BGD",
        "Bulgaria": "BGR",
        "Bahrain": "BHR",
        "Bahamas, The": "BHS",
        "Bosnia and Herzegovina": "BIH",
        "Belarus": "BLR",
        "Belize": "BLZ",
        "Bermuda": "BMU",
        "Bolivia": "BOL",
        "Brazil": "BRA",
        "Barbados": "BRB",
        "Brunei Darussalam": "BRN",
        "Bhutan": "BTN",
        "Botswana": "BWA",
        "Central African Republic": "CAF",
        "Canada": "CAN",
        "Switzerland": "CHE",
        "Chile": "CHL",
        "China": "CHN",
        "Cote d'Ivoire": "CIV",
        "Cameroon": "CMR",
        "Congo, Dem. Rep.": "COD",
        "Congo, Rep.": "COG",
        "Colombia": "COL",
        "Comoros": "COM",
        "Cabo Verde": "CPV",
        "Costa Rica": "CRI",
        "Cuba": "CUB",
        "Curacao": "CUW",
        "Cayman Islands": "CYM",
        "Cyprus": "CYP",
        "Czech Republic": "CZE",
        "Germany": "DEU",
        "Djibouti": "DJI",
        "Dominica": "DMA",
        "Denmark": "DNK",
        "Dominican Republic": "DOM",
        "Algeria": "DZA",
        "Ecuador": "ECU",
        "Egypt, Arab Rep.": "EGY",
        "Eritrea": "ERI",
        "Spain": "ESP",
        "Estonia": "EST",
        "Ethiopia": "ETH",
        "Finland": "FIN",
        "Fiji": "FJI",
        "France": "FRA",
        "Faroe Islands": "FRO",
        "Micronesia, Fed. Sts.": "FSM",
        "Gabon": "GAB",
        "United Kingdom": "GBR",
        "Georgia": "GEO",
        "Ghana": "GHA",
        "Gibraltar": "GIB",
        "Guinea": "GIN",
        "Gambia, The": "GMB",
        "Guinea-Bissau": "GNB",
        "Equatorial Guinea": "GNQ",
        "Greece": "GRC",
        "Grenada": "GRD",
        "Greenland": "GRL",
        "Guatemala": "GTM",
        "Guam": "GUM",
        "Guyana": "GUY",
        "Hong Kong SAR, China": "HKG",
        "Honduras": "HND",
        "Croatia": "HRV",
        "Haiti": "HTI",
        "Hungary": "HUN",
        "Indonesia": "IDN",
        "Isle of Man": "IMN",
        "India": "IND",
        "Ireland": "IRL",
        "Iran, Islamic Rep.": "IRN",
        "Iraq": "IRQ",
        "Iceland": "ISL",
        "Israel": "ISR",
        "Italy": "ITA",
        "Jamaica": "JAM",
        "Jordan": "JOR",
        "Japan": "JPN",
        "Kazakhstan": "KAZ",
        "Kenya": "KEN",
        "Kyrgyz Republic": "KGZ",
        "Cambodia": "KHM",
        "Kiribati": "KIR",
        "St. Kitts and Nevis": "KNA",
        "Korea, Rep.": "KOR",
        "Kosovo": "KOS",
        "Kuwait": "KWT",
        "Lao PDR": "LAO",
        "Lebanon": "LBN",
        "Liberia": "LBR",
        "Libya": "LBY",
        "St. Lucia": "LCA",
        "Liechtenstein": "LIE",
        "Sri Lanka": "LKA",
        "Lesotho": "LSO",
        "Lithuania": "LTU",
        "Luxembourg": "LUX",
        "Latvia": "LVA",
        "Macao SAR, China": "MAC",
        "St. Martin (French part)": "MAF",
        "Monaco": "MAN",
        "Morocco": "MAR",
        "Moldova": "MDA",
        "Madagascar": "MDG",
        "Maldives": "MDV",
        "Mexico": "MEX",
        "Marshall Islands": "MHL",
        "North Macedonia": "MKD",
        "Mali": "MLI",
        "Malta": "MLT",
        "Myanmar": "MMR",
        "Montenegro": "MNE",
        "Mongolia": "MNG",
        "Northern Mariana Islands": "MNP",
        "Mozambique": "MOZ",
        "Mauritania": "MRT",
        "Mauritius": "MUS",
        "Malawi": "MWI",
        "Malaysia": "MYS",
        "Namibia": "NAM",
        "New Caledonia": "NCL",
        "Niger": "NER",
        "Nigeria": "NGA",
        "Nicaragua": "NIC",
        "Netherlands": "NLD",
        "Norway": "NOR",
        "Nepal": "NPL",
        "Nauru": "NRU",
        "New Zealand": "NZL",
        "Oman": "OMN",
        "Pakistan": "PAK",
        "Panama": "PAN",
        "Peru": "PER",
        "Philippines": "PHL",
        "Palau": "PLW",
        "Papua New Guinea": "PNG",
        "Poland": "POL",
        "Puerto Rico": "PRI",
        "Portugal": "PRT",
        "Korea, Dem. People's Rep.": "PRK",
        "Paraguay": "PRY",
        "West Bank and Gaza": "PSE",
        "French Polynesia": "PYF",
        "Qatar": "QAT",
        "Romania": "ROU",
        "Russian Federation": "RUS",
        "Rwanda": "RWA",
        "Saudi Arabia": "SAU",
        "Sudan": "SDN",
        "Senegal": "SEN",
        "Singapore": "SGP",
        "Solomon Islands": "SLB",
        "Sierra Leone": "SLE",
        "El Salvador": "SLV",
        "San Marino": "SMR",
        "Somalia": "SOM",
        "Serbia": "SRB",
        "South Sudan": "SSD",
        "Sao Tome and Principe": "STP",
        "Suriname": "SUR",
        "Slovak Republic": "SVK",
        "Slovenia": "SVN",
        "Sweden": "SWE",
        "Eswatini": "SWZ",
        "Sint Maarten (Dutch part)": "SXM",
        "Seychelles": "SYC",
        "Syrian Arab Republic": "SYR",
        "Turks and Caicos Islands": "TCA",
        "Chad": "TCD",
        "Togo": "TGO",
        "Thailand": "THA",
        "Tajikistan": "TJK",
        "Turkmenistan": "TKM",
        "Timor-Leste": "TLS",
        "Tonga": "TON",
        "Trinidad and Tobago": "TTO",
        "Tunisia": "TUN",
        "Turkey": "TUR",
        "Tuvalu": "TWN",
        "Tanzania": "TZA",
        "Uganda": "UGA",
        "Ukraine": "UKR",
        "Uruguay": "URY",
        "United States": "USA",
        "Uzbekistan": "UZB",
        "St. Vincent and the Grenadines": "VCT",
        "Venezuela, RB": "VEN",
        "British Virgin Islands": "VGB",
        "Virgin Islands (U.S.)": "VIR",
        "Vietnam": "VNM",
        "Vanuatu": "VUT",
        "American Samoa": "WSM",
        "Yemen, Rep.": "YEM",
        "South Africa": "ZAF",
        "Zambia": "ZMB",
        "Zimbabwe": "ZWE"
    }

    missing = set()
    lines = []
    with open("population_total.csv", "r") as file:
        reader = csv.reader(file, delimiter=",", quotechar="\"")
        for row in reader:
            if row[0] not in population_total_names_to_codes:
                missing.add(row[0])
            else:
                lines.append("\"{}\",\"{}\",\"{}\"\n".format(
                    population_total_names_to_codes[row[0]], row[1], row[2]))

    print("Missing countries:")
    for m in missing:
        print(m)

    with open("population_total.csv", "w") as file:
        for line in lines:
            file.write(line)

# Splitting up the first datasets long rows into smaller tuples
# split_up_long_tuples("fossil_fuel_consumption.csv")
# split_up_long_tuples("gdp.csv")
# split_up_long_tuples("population_growth.csv")

# Getting the initial country code table
# get_one_country_table("co2_emission.csv")

# Print the countries from this table
# print_population_total_countries()
# After this, inspect it and match the country names with country codes using the link
# https://www.iban.com/country-codes and the country_codes.csv. If needed, add new country codes
# s.t. we can later convert the names to codes


# Replace the country names to codes from population_total.csv
population_total_replace_country_names()
